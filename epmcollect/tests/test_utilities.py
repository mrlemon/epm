from django.test import TestCase

from datetime import date

from epmcollect.models import Project, RAG, Quarter, BICCReturn, Person, Role, DeliveryConfidenceAssessment
from epmcollect.utils import ready_graph_context_for_json


class TestUtilities(TestCase):
    def setUp(self):
        self.project = Project(name="A1 Roundhouse", sro_rating=RAG(pk=1))
        self.person = Person(first_name="Smeke Smeddnom", last_name="Keithley")
        self.rag = RAG(rag="GREEN")
        self.p1 = Project(name="Test Project", sro_rating=RAG(rag="RED"),
                     sro=Person(first_name="Colin", last_name="Webb", role=Role(name="Fixer")))
        self.f1 = Quarter(
            qref='1', yearref='2012', start=date(
                2012, 4, 1), end=date(
                2012, 6, 30))
        self.all_quarters = Quarter.objects.all()
        self.dca = DeliveryConfidenceAssessment(
            assessor=self.person,
            date_assessed=date.today(),
            project=self.p1,
            quarter=self.f1,
            rag=self.rag)
        self.b_return1 = BICCReturn(sro_delivery_confidence_rag=self.dca)

    def test_new_project(self):
        self.assertEqual(self.project.name, "A1 Roundhouse")

    def test_create_new_rag(self):
        rag = RAG(rag="ORANGE")
        self.assertEqual(rag.rag, "ORANGE")

    def test_graph_for_context(self):
        dict_param = {'AMBER/RED': 6}
        list_param = {'label': 'q12014'}
        result = ready_graph_context_for_json(dict_param, list_param)
        self.assertEqual(result, {'amber_red_q12014': 6, 'AMBER/RED': 6})
