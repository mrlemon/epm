import time
from selenium import webdriver

from django.test import TestCase


class NewVisitorTest(TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(4)

    def tearDown(self):
        self.browser.quit()

    def test_presence_of_log_in_page(self):
        # John has been emailed a username and password from the Portfolio Team.
        # He has been told to go to http://localhost.com:8000/epmcollect/login to log in
        self.browser.get('http://localhost:8000/epmcollect/login')

        # On that page, it simply has a log in form and the page title
        self.assertIn('Evolved Portfolio Management (EPM)', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('EPM - Log in', header_text)

    def test_username_password_entry(self):
        self.browser.get('http://localhost:8000/epmcollect/login')
        # John enters his username into the username field
        # his username is 'john', his password is 'password'
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        # we are using john as a test user - he is created in the populate script
        username_field.send_keys("john")
        password_field.send_keys("password")
        # submit using WebDriver convenience method
        password_field.submit()
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Welcome to EPM!', header_text)
        welcome_message = self.browser.find_element_by_tag_name('p').text
        self.assertIn('Welcome john', welcome_message)

    def test_for_list_of_projects_on_home_page(self):
        # john goes to the login page and enters his credensh
        self.browser.get('http://localhost:8000/epmcollect/login')
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        username_field.send_keys("john")
        password_field.send_keys("password")
        # submit using WebDriver convenience method
        password_field.submit()
        # he notices there is a table containing links to the projects he is the owner for
        # now we look for a div called 'project-list' and find an H3 in it
        project_list_div_header = self.browser.find_element_by_id('project-list').find_element_by_tag_name('h3').text
        self.assertIn('Projects owned by john', project_list_div_header)
        project_list_table_td = self.browser.find_element_by_xpath(
            '//*[@id="project-list-table"]/tbody/tr[2]/td/a').text
        self.assertIn('Chopper Station on GMH', project_list_table_td)

    def test_single_project_page(self):
        # john goes to the login page and enters his credensh
        self.browser.get('http://localhost:8000/epmcollect/login')
        username_field = self.browser.find_element_by_id('id_username')
        password_field = self.browser.find_element_by_id('id_password')
        username_field.send_keys("john")
        password_field.send_keys("password")
        # submit using WebDriver convenience method
        password_field.submit()
        first_project_link = self.browser.find_element_by_xpath(
            '//*[@id="project-list-table"]/tbody/tr[2]/td/a')
        first_project_link.click()
        first_dca_return_in_table = self.browser.find_element_by_xpath(
            '/html/body/table/tbody/tr[2]/td[1]/a'
        ).text
        self.assertIn('BICC Return: Q1/2012', first_dca_return_in_table)


class DeliveryConfidenceAssessmentForm(TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(4)

    def tearDown(self):
        self.browser.quit()

    def test_page_form_presence(self):
        self.browser.get('http://localhost:8000/dca/new')
        header = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('Submit a new Delivery Confidence Assessment', header)

