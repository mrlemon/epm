import random
from datetime import date

from django.test import TestCase

from epmcollect.graphics import stacked_column
from epmcollect.models import Project, ProjectStage, RAG, Role, Portfolio, ProjectMethodology, Person, Quarter, \
    DeliveryConfidenceAssessment, BICCReturn

# Some utility functions, pulled from the populate.py script:
def create_sro_delivery_confidence_rag(proj, quart):
    """Use this if you want to apply random RAGs to your DCAs. Otherwise use _constant version."""
    sdc = DeliveryConfidenceAssessment(project_id=proj.id,
                                       quarter_id=quart.id,
                                       date_assessed=date.today(),
                                       rag_id=random.choice([1, 2, 3, 4, 5]),
                                       granted=True,
                                       assessor_id=random.choice([1, 2, 3, 4, 5]))
    sdc.save()
    return sdc.id


def create_sro_delivery_confidence_rag_constant(proj, quart):
    """Use this if you need a fixed RAG for each DCA. Otherwise use the non-_constant version."""
    sdc = DeliveryConfidenceAssessment(project_id=proj.id,
                                       quarter_id=quart.id,
                                       date_assessed=date.today(),
                                       rag_id=1,  # for this, we want GREENs all the way
                                       granted=True,
                                       assessor_id=random.choice([1, 2, 3, 4, 5]))
    sdc.save()
    return sdc.id


class GraphicsData(TestCase):
    """Currently doing a lot of set up until I write proper fixtures."""

    def setUp(self):
        # this allows full readout of traceback after failure
        self.maxDiff = None

        # a project methodology
        self.pm1 = ProjectMethodology(methodology="Waterfall")
        self.pm1.save()
        self.pm2 = ProjectMethodology(methodology="Agile")
        self.pm2.save()
        self.pm3 = ProjectMethodology(methodology="Hybrid")
        self.pm3.save()

        # project stages
        self.ps1 = ProjectStage(name='Concept', description='')
        self.ps2 = ProjectStage(name='Feasibility', description='')
        self.ps3 = ProjectStage(name='Appraise and Select', description='')
        self.ps4 = ProjectStage(name='Define and Refine Plan', description='')
        self.ps5 = ProjectStage(name='Execute', description='')
        self.ps6 = ProjectStage(name='Operate', description='')
        self.ps7 = ProjectStage(name='Embed in BAU. Review and Lessons Learned', description='')
        self.ps8 = ProjectStage(name='Completion Benefits Realisation (steady state)', description='')
        self.ps9 = ProjectStage(name='Project Initialisation', description='')
        self.ps1.save()
        self.ps2.save()
        self.ps3.save()
        self.ps4.save()
        self.ps5.save()
        self.ps6.save()
        self.ps7.save()
        self.ps8.save()
        self.ps9.save()

        # a Portfolio
        self.port1 = Portfolio(name="This Big Portfolio")
        self.port1.save()

        # a Roles
        self.role1 = Role(name="Ear waxer")
        self.role1.save()

        # a couple of Persons
        self.person1 = Person(first_name="Harold", last_name="Smyth", role=self.role1)
        self.person2 = Person(first_name="Clithero", last_name="Hollins", role=self.role1)
        self.person1.save()
        self.person2.save()

        # some RAGs
        self.rag1 = RAG(rag="GREEN")
        self.rag2 = RAG(rag="AMBER/GREEN")
        self.rag3 = RAG(rag="AMBER")
        self.rag4 = RAG(rag="AMBER/RED")
        self.rag5 = RAG(rag="RED")
        self.rag1.save()
        self.rag2.save()
        self.rag3.save()
        self.rag4.save()
        self.rag5.save()

        # some Quarters
        f1 = Quarter(
            qref='1', yearref='2012', start=date(
                2012, 4, 1), end=date(
                2012, 6, 30))
        f2 = Quarter(
            qref='2', yearref='2012', start=date(
                2012, 7, 1), end=date(
                2012, 9, 30))
        f3 = Quarter(
            qref='3', yearref='2012', start=date(
                2012, 10, 1), end=date(
                2012, 12, 31))
        f4 = Quarter(
            qref='4', yearref='2012', start=date(
                2013, 1, 1), end=date(
                2013, 3, 31))
        f5 = Quarter(
            qref='1', yearref='2013', start=date(
                2013, 4, 1), end=date(
                2013, 6, 30))
        f6 = Quarter(
            qref='2', yearref='2013', start=date(
                2013, 7, 1), end=date(
                2013, 9, 30))
        f7 = Quarter(
            qref='3', yearref='2013', start=date(
                2013, 10, 1), end=date(
                2013, 12, 31))
        f8 = Quarter(
            qref='4', yearref='2013', start=date(
                2014, 1, 1), end=date(
                2014, 3, 31))
        f9 = Quarter(
            qref='1', yearref='2014', start=date(
                2014, 4, 1), end=date(
                2014, 6, 30))
        f10 = Quarter(
            qref='2', yearref='2014', start=date(
                2014, 7, 1), end=date(
                2014, 9, 30))
        f11 = Quarter(
            qref='3', yearref='2014', start=date(
                2014, 10, 1), end=date(
                2014, 12, 31))
        f12 = Quarter(
            qref='4', yearref='2014', start=date(
                2014, 1, 1), end=date(
                2015, 3, 31))
        f13 = Quarter(
            qref='1', yearref='2015', start=date(
                2015, 4, 1), end=date(
                2015, 6, 30))
        f14 = Quarter(
            qref='2', yearref='2015', start=date(
                2015, 7, 1), end=date(
                2015, 9, 30))
        f15 = Quarter(
            qref='3', yearref='2015', start=date(
                2015, 10, 1), end=date(
                2015, 12, 31))
        f16 = Quarter(
            qref='4', yearref='2015', start=date(
                2016, 1, 1), end=date(
                2016, 3, 31))
        f1.save()
        f2.save()
        f3.save()
        f4.save()
        f5.save()
        f6.save()
        f7.save()
        f8.save()
        f9.save()
        f10.save()
        f11.save()
        f12.save()
        f13.save()
        f14.save()
        f15.save()
        f16.save()

        # let's set up some test projects
        #        self.p1 = Project(name="Test Project 1", sro_rating=self.rag5, sro=self.person1, methodology_id=random.choice([1,2,3]), stage_id=random.choice([1,8]))
        #        self.p2 = Project(name="Test Project 2", sro_rating=self.rag4, sro=self.person1, methodology_id=random.choice([1,2,3]), stage_id=random.choice([1,8]))
        #        self.p3 = Project(name="Test Project 3", sro_rating=self.rag3, sro=self.person1, methodology_id=random.choice([1,2,3]), stage_id=random.choice([1,8]))
        #        self.p4 = Project(name="Test Project 4", sro_rating=self.rag2, sro=self.person1, methodology_id=random.choice([1,2,3]), stage_id=random.choice([1,8]))
        #        self.p5 = Project(name="Test Project 5", sro_rating=self.rag1, sro=self.person1, methodology_id=random.choice([1,2,3]), stage_id=random.choice([1,8]))
        self.p1 = Project(name="Test Project 1", sro_rating=self.rag5, sro=self.person1,
                          methodology_id=random.choice([1, 2, 3]),
                          stage_id=1)  # we only want to test 'Concept' ProjectStage
        self.p2 = Project(name="Test Project 2", sro_rating=self.rag4, sro=self.person1,
                          methodology_id=random.choice([1, 2, 3]), stage_id=1)
        self.p3 = Project(name="Test Project 3", sro_rating=self.rag3, sro=self.person1,
                          methodology_id=random.choice([1, 2, 3]), stage_id=1)
        self.p4 = Project(name="Test Project 4", sro_rating=self.rag2, sro=self.person1,
                          methodology_id=random.choice([1, 2, 3]), stage_id=1)
        self.p5 = Project(name="Test Project 5", sro_rating=self.rag1, sro=self.person1,
                          methodology_id=random.choice([1, 2, 3]), stage_id=1)
        self.p1.save()
        self.p1.portfolios.add(self.port1)
        self.p2.save()
        self.p2.portfolios.add(self.port1)
        self.p3.save()
        self.p3.portfolios.add(self.port1)
        self.p4.save()
        self.p4.portfolios.add(self.port1)
        self.p5.save()
        self.p5.portfolios.add(self.port1)
        self.port1.save()

        # let's make some BICC returns
        all_quarters = Quarter.objects.all()
        all_projects = Project.objects.all()
        returns = [
            BICCReturn(sro_delivery_confidence_rag_id=create_sro_delivery_confidence_rag_constant(project, quarter))
            for project in all_projects for quarter in all_quarters]
        for r in returns:
            r.save()

    def test_delivery_confidence_against_project_maturity(self):
        """This test checks whether we are getting the right Python dictionary we need to build
        the Confidence Against Project Maturity Graph from the BICC Report Dashboard."""

        ## the set-up for this uses DCAs whose RAG ratings are all green and all the projects are at 'Concept" stage

        output_json_dict = {'Define and Refine Plan': {'green_label': 'GREEN', 'amber_green_label': 'AMBER/GREEN',
                                                       'amber_label': 'AMBER', 'amber_red_label': 'AMBER/RED',
                                                       'red_label': 'RED'},
                            'Operate': {'green_label': 'GREEN', 'amber_green_label': 'AMBER/GREEN',
                                        'amber_label': 'AMBER', 'amber_red_label': 'AMBER/RED', 'red_label': 'RED'},
                            'Concept': {'green_label': 'GREEN', 'amber_red_label': 'AMBER/RED', 'amber_label': 'AMBER',
                                        'amber_green_label': 'AMBER/GREEN', 'GREEN': 5, 'red_label': 'RED'},
                            'Execute': {'green_label': 'GREEN', 'amber_green_label': 'AMBER/GREEN',
                                        'amber_label': 'AMBER', 'amber_red_label': 'AMBER/RED', 'red_label': 'RED'},
                            'Embed in BAU. Review and Lessons Learned': {'green_label': 'GREEN',
                                                                         'amber_green_label': 'AMBER/GREEN',
                                                                         'amber_label': 'AMBER',
                                                                         'amber_red_label': 'AMBER/RED',
                                                                         'red_label': 'RED'},
                            'Feasibility': {'green_label': 'GREEN', 'amber_green_label': 'AMBER/GREEN',
                                            'amber_label': 'AMBER', 'amber_red_label': 'AMBER/RED', 'red_label': 'RED'},
                            'Project Initialisation': {'green_label': 'GREEN', 'amber_green_label': 'AMBER/GREEN',
                                                       'amber_label': 'AMBER', 'amber_red_label': 'AMBER/RED',
                                                       'red_label': 'RED'},
                            'Completion Benefits Realisation (steady state)': {'green_label': 'GREEN',
                                                                               'amber_green_label': 'AMBER/GREEN',
                                                                               'amber_label': 'AMBER',
                                                                               'amber_red_label': 'AMBER/RED',
                                                                               'red_label': 'RED'},
                            'Appraise and Select': {'green_label': 'GREEN', 'amber_green_label': 'AMBER/GREEN',
                                                    'amber_label': 'AMBER', 'amber_red_label': 'AMBER/RED',
                                                    'red_label': 'RED'}}

        q = Quarter.objects.get(pk=16)
        g = stacked_column.DCAProjectMaturity(q)
        self.assertEquals(g.generate_graph_data(), output_json_dict)

