import random
from datetime import date

from django.contrib.auth.models import User
from django.test import TestCase

from epmcollect.models import Quarter, BICCReturn, Project, RAG, Person, Role, Portfolio, DeliveryConfidenceAssessment
from epmcollect.models import ProjectMethodology


class TestModels(TestCase):
    def setUp(self):
        # this allows full readout of traceback after failure
        self.maxDiff = None

        # let's make sure we have a User
        self.user1 = User.objects.create_user('john', 'john@bobbins.com', 'password')

        # a project methodology
        self.pm1 = ProjectMethodology(methodology="Waterfall")
        self.pm1.save()
        self.pm2 = ProjectMethodology(methodology="Agile")
        self.pm2.save()
        self.pm3 = ProjectMethodology(methodology="Hybrid")
        self.pm3.save()

        # a Portfolio
        self.port1 = Portfolio(name="This Big Portfolio")
        self.port1.save()

        # a Roles
        self.role1 = Role(name="Ear waxer")
        self.role1.save()

        # a couple of Persons
        self.person1 = Person(first_name="Harold", last_name="Smyth", role=self.role1)
        self.person2 = Person(first_name="Clithero", last_name="Hollins", role=self.role1)
        self.person1.save()
        self.person2.save()

        # some RAGs
        self.rag1 = RAG(rag="GREEN")
        self.rag2 = RAG(rag="AMBER/GREEN")
        self.rag3 = RAG(rag="AMBER")
        self.rag4 = RAG(rag="AMBER/RED")
        self.rag5 = RAG(rag="RED")
        self.rag1.save()
        self.rag2.save()
        self.rag3.save()
        self.rag4.save()
        self.rag5.save()

        # let's set up some test projects
        self.p1 = Project(name="Test Project 1", sro_rating=self.rag5, sro=self.person1, methodology_id=random.choice([1,2,3]))
        self.p2 = Project(name="Test Project 2", sro_rating=self.rag4, sro=self.person1, methodology_id=random.choice([1,2,3]))
        self.p3 = Project(name="Test Project 3", sro_rating=self.rag3, sro=self.person1, methodology_id=random.choice([1,2,3]))
        self.p4 = Project(name="Test Project 4", sro_rating=self.rag2, sro=self.person1, methodology_id=random.choice([1,2,3]))
        self.p5 = Project(name="Test Project 5", sro_rating=self.rag1, sro=self.person1, methodology_id=random.choice([1,2,3]))
        self.p1.save()
        self.p1.portfolios.add(self.port1)
        self.p2.save()
        self.p2.portfolios.add(self.port1)
        self.p3.save()
        self.p3.portfolios.add(self.port1)
        self.p4.save()
        self.p4.portfolios.add(self.port1)
        self.p5.save()
        self.p5.portfolios.add(self.port1)
        self.port1.save()

        # a single quarter
        self.q = Quarter(qref='1', yearref='2012', start=date(2012, 4, 1), end=date(2012, 6, 30))
        self.q.save()

        # some delivery confidence assessments are required
        self.dca1 = DeliveryConfidenceAssessment(
            assessor=self.person1,
            date_assessed=date.today(),
            project=self.p1,
            quarter=self.q,
            granted=True,
            rag=self.rag1)
        self.dca2 = DeliveryConfidenceAssessment(
            assessor=self.person2,
            date_assessed=date.today(),
            project=self.p2,
            granted=True,
            quarter=self.q,
            rag=self.rag2)
        self.dca3 = DeliveryConfidenceAssessment(
            assessor=self.person1,
            date_assessed=date.today(),
            project=self.p3,
            granted=True,
            quarter=self.q,
            rag=self.rag3)
        self.dca4 = DeliveryConfidenceAssessment(
            assessor=self.person2,
            date_assessed=date.today(),
            project=self.p4,
            granted=True,
            quarter=self.q,
            rag=self.rag4)
        self.dca5 = DeliveryConfidenceAssessment(
            assessor=self.person1,
            date_assessed=date.today(),
            project=self.p5,
            granted=True,
            quarter=self.q,
            rag=self.rag5)
        self.dca1.save()
        self.dca2.save()
        self.dca3.save()
        self.dca4.save()
        self.dca5.save()

        # some returns for a single quarter
        self.b_return1 = BICCReturn(sro_delivery_confidence_rag=self.dca1)
        self.b_return2 = BICCReturn(sro_delivery_confidence_rag=self.dca2)
        self.b_return3 = BICCReturn(sro_delivery_confidence_rag=self.dca3)
        self.b_return4 = BICCReturn(sro_delivery_confidence_rag=self.dca4)
        self.b_return5 = BICCReturn(sro_delivery_confidence_rag=self.dca5)
        self.b_return1.save()
        self.b_return2.save()
        self.b_return3.save()
        self.b_return4.save()
        self.b_return5.save()

    def test_quarter_created(self):
        self.assertIsInstance(self.q, Quarter, msg="{} is a Quarter".format(self.q))

    def test_bicc_return_created(self):
        self.assertIsInstance(self.b_return1, BICCReturn)

    def test_get_bicc_returns(self):
        self.assertIsInstance(self.q.epmcollect_deliveryconfidenceassessment_related.first().biccreturn_set.first(), BICCReturn)

    def test_get_quarter_label(self):
        self.assertEqual('Q1/2012', self.q.get_label())

    def test_rag_count_simple(self):
        d = {'AMBER/GREEN': 1, 'RED': 1, 'AMBER/RED': 1, 'AMBER': 1, 'GREEN': 1}
        self.assertEqual(d, self.q.get_rag_count())

    def test_rag_count_jsonify_switch(self):
        d = {'AMBER': 1, 'amber_red_label': 'AMBER/RED', 'AMBER/GREEN': 1, 'start': '2012-04-01',
                         'end': '2012-06-30', 'amber_red_q12012': 1, 'label': 'Q1/2012', 'red_label': 'RED',
                         'amber_q12012': 1, 'RED': 1, 'GREEN': 1, 'amber_green_q12012': 1, 'AMBER/RED': 1,
                         'green_q12012': 1, 'red_q12012': 1, 'green_label': 'GREEN',
                         'amber_green_label': 'AMBER/GREEN', 'amber_label': 'AMBER'}
        baws = self.q.get_rag_count(jsonify=True)
        self.assertEqual(d, baws)

    def test_portfolio_rag_counts(self):
        self.assertEqual(self.port1.greens, 1)
        self.assertEqual(self.port1.amber_greens, 1)
        self.assertEqual(self.port1.ambers, 1)
        self.assertEqual(self.port1.amber_reds, 1)
        self.assertEqual(self.port1.reds, 1)

    def test_portfolio_name(self):
        self.assertEqual(str(self.port1), 'This Big Portfolio')

    def test_role_name(self):
        self.assertEqual(str(self.role1), 'Ear waxer')

    def test_get_person_full_name(self):
        self.assertEqual(str(self.person1), 'Harold Smyth')
        self.assertEqual(self.person1.full_name, 'Harold Smyth')

    def test_get_project_person_works_on(self):
        self.assertEqual(self.person1.project, 'Test Project 1')

    def test_get_rag_name(self):
        self.assertEqual(str(self.rag1), 'GREEN')

    def test_get_projectmethodology(self):
        self.assertEqual(str(self.pm1), 'Waterfall')


