from django.apps import AppConfig


class EpmcontrolConfig(AppConfig):
    name = 'epmcollect'
    verbose_name = 'EPM Control'
