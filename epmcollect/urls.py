from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'epmcollect'

urlpatterns = [
    # /epmcollect/
    url(r'^$', views.index, name='index'),

    # /epmcollect/thanks - for generic thanking
    url(r'^thanks/$', views.thanks, name='thanks'),

    # /epmcollect/projects
    url(r'^projects/$', views.ProjectListView.as_view(), name='projects'),
    # /epmcollect/projects/24
    url(r'^projects/(?P<pk>[0-9]+)/$', views.ProjectDetailView.as_view(), name='single_project'),

    # /epmcollect/bicc_returns/
    url(r'^bicc_returns/$', views.BICCReturnList.as_view(template_name='epmcollect/bicc_returns.html')),
    # /epmcollect/bicc_returns/10
    url(r'^bicc_returns/(?P<pk>[0-9]+)/$',
        views.BICCReturnDetailView.as_view(template_name='epmcollect/bicc_return.html'), name='bicc_return'),
    # /epm/collect/bicc_returns/preview/project/20
    url(r'^bicc_returns/preview/project/(?P<project_id>[0-9]+)/$', views.bicc_pre_submit_summary,
        name='bicc_pre_submit_summary'),
    # /epmcollect/bicc_returns/add
    url(r'^bicc_returns/new/$', views.bicc_new, name='bicc_new'),

    #/epmcollect/dca/23
    url(r'dca/(?P<pk>[0-9]+)/$', views.DeliveryConfidenceAssessmentView.as_view(), name='dca'),
    # # /epmcollect/dca/add
    # url(r'dca/add/$', views.DeliveryConfidenceAssessmentCreate.as_view(), name='dca-add'),
    # /epmcollect/dca/add
    url(r'^dca/add/project/(?P<project_id>[0-9]+)/$', views.dca_add, name='dca-add'),

    url(r'^graphic/stacked_column/1$', views.aggregate_returns_rag_per_quarter, name='aggregrate_graph'),
    url(r'^graphic/stacked_column/2/(?P<yearref>[0-9]+)/(?P<qref>[0-9]+)$', views.delivery_confidence_against_project_maturity, name='dca_project_maturity'),
    url(r'^login/$', auth_views.login, name='login')
]