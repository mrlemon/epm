"""
How to run this populate script properly:

- Delete all numbers .py files in migrations folder (leave __init__)
- Delete SQLite db file
- python manage.py makemigrations
- python manage.py migrate
- python manage.py createsuperuser
- python manage.py populate
You will then have a fresh database, migrations and set of data to work with. This will need to be
added to as development progresses.
"""
import random
from datetime import date

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from epmcollect.models import Quarter, RAG, Role, Person, Portfolio, Project, BICCReturn, DeliveryConfidenceAssessment, \
    ProjectMethodology, ProjectStage, DeliveryStructure, OrganisationType, Organisation,\
    OrganisationDivision,OrganisationLocation, OrganisationGroup, OrganisationTeam, OrganisationTeamType


class Command(BaseCommand):
    args = ''
    help = 'Populate the database with some initial data.'

    def _create_user(self):
        # let's make sure we have a User
        user1 = User.objects.create_user('john', 'john@bobbins.com', 'password')
        user1.save()

    def _create_organisation_type(self):
        org_t1 = OrganisationType(name='Government Department', description='A Government Department')
        org_t2 = OrganisationType(name='DfT Agency', description='An Agency of DfT')
        org_t3 = OrganisationType(name='Arms Length Body', description='An Arms Length Body')
        org_t4 = OrganisationType(name='Private Company', description='A private company')
        org_t5 = OrganisationType(name='Charity', description='A charity')
        print("Creating {}".format(org_t1))
        print("Creating {}".format(org_t2))
        print("Creating {}".format(org_t3))
        print("Creating {}".format(org_t4))
        print("Creating {}".format(org_t5))
        org_t1.save()
        org_t2.save()
        org_t3.save()
        org_t4.save()
        org_t5.save()

    def _create_organisation(self):
        org1 = Organisation(organisation_type_id=1, name='Department for Transport', description='The biggie')
        org2 = Organisation(organisation_type_id=2, name='Highways Agency', description='A little agency')
        org3 = Organisation(organisation_type_id=2, name='Countryside Commission', description='Small org')
        org4 = Organisation(organisation_type_id=3, name='Long Arms Ltd', description='An arms length body of some description')
        org5 = Organisation(organisation_type_id=4, name='Colin Cavie Building Ltd', description='A builder')
        print("Creating {}".format(org1))
        print("Creating {}".format(org2))
        print("Creating {}".format(org3))
        print("Creating {}".format(org4))
        print("Creating {}".format(org5))
        org1.save()
        org2.save()
        org3.save()
        org4.save()
        org5.save()

    def _create_organisation_location(self):
        org_loc1 = OrganisationLocation(
            organisation_id=1,
            address1='Great Misery House',
            address2='123 Horseberry Road',
            address3='Westminster',
            address4='London',
            address5='UK',
            postcode='SW1G 4PT'
        )
        org_loc2 = OrganisationLocation(
            organisation_id=2,
            address1='Highways House',
            address2='18 Cardinal Place',
            address3='Stenfield Largos',
            address4='Milton Hammersteign',
            postcode='GM12 8TQ'
        )
        org_loc3 = OrganisationLocation(
            organisation_id=3,
            address1='191 Leatherbridge Avenue',
            address2='Steffingham Blaze',
            address3='East Buckinghamshire',
            postcode='T34 9XX'
        )
        org_loc4 = OrganisationLocation(
            organisation_id=1,
            address1='Ashworth House',
            address2='Grand Parade',
            address3='Hastings',
            address4='Sussex',
            postcode='SU12 9TT'
        )
        org_loc5 = OrganisationLocation(
            organisation_id=4,
            address1='Tenterfield House',
            address2='10 Gigglesfield Meadows',
            address3='Bompton',
            address4='Erdleugh',
            postcode='ST1 1JT'
        )
        org_loc6 = OrganisationLocation(
            organisation_id=5,
            address1='Steel Towers',
            address2='The Gladenburgh',
            address3='Savilston',
            address4='High Bucks',
            postcode='ST10 1SS'
        )
        print("Creating {}".format(org_loc1))
        print("Creating {}".format(org_loc2))
        print("Creating {}".format(org_loc3))
        print("Creating {}".format(org_loc4))
        print("Creating {}".format(org_loc5))
        print("Creating {}".format(org_loc6))
        org_loc1.save()
        org_loc2.save()
        org_loc3.save()
        org_loc4.save()
        org_loc5.save()
        org_loc6.save()

    def _create_organisation_group(self):
        org_group1 = OrganisationGroup(organisation_id=1, name='International, Strategy and Security', description='A Group')
        org_group2 = OrganisationGroup(organisation_id=1, name='Resources and Finance', description='A Group')
        print("Creating {}".format(org_group1))
        print("Creating {}".format(org_group2))
        org_group1.save()
        org_group2.save()

    def _create_organisation_division(self):
        org_div1 = OrganisationDivision(organisation_group_id=1, name='Major Roads Projects Division')
        org_div2 = OrganisationDivision(organisation_group_id=1, name='Rail Projects Division')
        print("Creating {}".format(org_div1))
        print("Creating {}".format(org_div2))
        org_div1.save()
        org_div2.save()

    def _create_organisation_team_type(self):
        org_team_type1 = OrganisationTeamType(name='Project Planning')
        org_team_type2 = OrganisationTeamType(name='Subportfolio Team')
        org_team_type3 = OrganisationTeamType(name='Finance Management')
        org_team_type4 = OrganisationTeamType(name='Legal')
        print("Creating {}".format(org_team_type1))
        print("Creating {}".format(org_team_type2))
        print("Creating {}".format(org_team_type3))
        print("Creating {}".format(org_team_type4))
        org_team_type1.save()
        org_team_type2.save()
        org_team_type3.save()
        org_team_type4.save()

    def _create_organisation_team(self):
        org_team1 = OrganisationTeam(organisation_division_id=1, name='Project Plannging Team', organisation_team_type_id=1)
        org_team2 = OrganisationTeam(organisation_division_id=2, name='Portfolio Management Team for DfT Group', organisation_team_type_id=2)
        org_team3 = OrganisationTeam(organisation_division_id=1, name='Team for Running Things', organisation_team_type_id=1)
        org_team4 = OrganisationTeam(organisation_division_id=3, name='The Penny Pinching Team', organisation_team_type_id=3)
        org_team5 = OrganisationTeam(organisation_division_id=4, name='Legalese and Contractual Diversion', organisation_team_type_id=4)
        print("Creating {}".format(org_team1))
        print("Creating {}".format(org_team2))
        print("Creating {}".format(org_team3))
        print("Creating {}".format(org_team4))
        print("Creating {}".format(org_team5))
        org_team1.save()
        org_team2.save()
        org_team3.save()
        org_team4.save()
        org_team5.save()

    def _create_delivery_structure(self):
        ds1 = DeliveryStructure(name='Project', description='A project is something that has an end.')
        ds2 = DeliveryStructure(name='Programme', description='A programme is endless.')
        ds3 = DeliveryStructure(name='Other', description='A delivery structure that isn\'t project or programme')
        ds1.save()
        ds2.save()
        ds3.save()
        print("Created Delivery Structure entities: {}, {} and {}".format(ds1, ds2, ds3))

    def _create_project_stages(self):
        ps1 = ProjectStage(name='Concept', description='')
        ps2 = ProjectStage(name='Feasibility', description='')
        ps3 = ProjectStage(name='Appraise and Select', description='')
        ps4 = ProjectStage(name='Define and Refine Plan', description='')
        ps5 = ProjectStage(name='Execute', description='')
        ps6 = ProjectStage(name='Operate', description='')
        ps7 = ProjectStage(name='Embed in BAU. Review and Lessons Learned', description='')
        ps8 = ProjectStage(name='Completion Benefits Realisation (steady state)', description='')
        ps9 = ProjectStage(name='Project Initialisation', description='')
        print("Creating {} Project Stage".format(ps1))
        print("Creating {} Project Stage".format(ps2))
        print("Creating {} Project Stage".format(ps3))
        print("Creating {} Project Stage".format(ps4))
        print("Creating {} Project Stage".format(ps5))
        print("Creating {} Project Stage".format(ps6))
        print("Creating {} Project Stage".format(ps7))
        print("Creating {} Project Stage".format(ps8))
        print("Creating {} Project Stage".format(ps9))
        ps1.save()
        ps2.save()
        ps3.save()
        ps4.save()
        ps5.save()
        ps6.save()
        ps7.save()
        ps8.save()
        ps9.save()


    def _create_project_methodologies(self):
        pm1 = ProjectMethodology(methodology='Waterfall')
        pm2 = ProjectMethodology(methodology='Agile')
        pm3 = ProjectMethodology(methodology='Hybrid')
        print("Creating {}".format(pm1))
        print("Creating {}".format(pm2))
        print("Creating {}".format(pm3))
        pm1.save()
        pm2.save()
        pm3.save()


    def _create_roles(self):
        r1 = Role(name="Project Manager")
        r1.save()
        r2 = Role(name="Senior Responsible Officer (SRO)")
        r2.save()
        r3 = Role(name="Technical Manager")
        r3.save()
        r4 = Role(name="Team Leader")
        r4.save()
        r5 = Role(name="PMO Liaison")
        r5.save()

    def _create_persons(self):
        first_names = [
            "Mark",
            "Colin",
            "Steve",
            "Martha",
            "Gemma",
            "Claire",
            "October",
            "Briony",
            "Spellton",
            "David",
            "Clare",
            "Oberon",
            "Phillip",
            "William",
            "Michelle",
            "Aitken",
        ]
        last_names = [
            "Ceavis",
            "Helter",
            "Smeddum",
            "Smith-and-Wesson",
            "Terrence",
            "Goagles",
            "Heaslip-Carnivoria",
            "Philis-Tronk",
            "Jessles",
            "Babberage",
            "Stennars",
            "Smithage",
            "Kensington-Loath",
            "Heaver",
            "McConnell",
            "Bridgington-Stanndard",
            "Oaley",
            "Lemon",
            "Cooper",
            "Festering",
            "Botti"
        ]
        for p in range(40):
            p = Person(
                first_name=random.choice(first_names),
                last_name=random.choice(last_names),
                team_id=random.choice(range(1, 5)),
                role_id=random.choice([1, 2, 3, 4, 5]),
                landline='0207 345 2312',
                email='email@randomplace.com',
                email_alt='email_alt@randomplace.com'
            )
            print(p)
            p.save()


    def _create_portfolios(self):
        p1 = Portfolio(name="DfT Tier 1 Projects")
        p2 = Portfolio(name="DfT Tier 2 Projects")
        p1.save()
        p2.save()
        print("Adding portfolios")


    def _create_rags(self):
        green = RAG(rag="GREEN")
        amber_green = RAG(rag="AMBER/GREEN")
        amber = RAG(rag="AMBER")
        amber_red = RAG(rag="AMBER/RED")
        red = RAG(rag="RED")
        green.save()
        amber_green.save()
        amber.save()
        amber_red.save()
        red.save()
        print("Adding RAGs")


    def _create_projects(self):
        project_random = [
            "Slipjet Construction Ltd",
            "Foster Homes Across Moldovia",
            "Ridding the World of Formula 3000",
            "Mark Stennford Hairdressing",
            "A12 Rounadabout - Make Them Hexagons",
            "Cumbrian Airways Air Crash Investigation",
            "Travelling Wilberries CDs - The Big Search",
            "Evian Is Found To Be Poison",
            "Eggard to Hemptoast Truckway",
            "Tarmacing the Southern Wastelands",
            "Shuttlecock Shuttlebuses Ltd",
            "Government Takeover - Lewisham Chipshops",
            "Bistel to Chipsked Dual Carriageway Extension",
            "Tempworth Trollypark and Buggy Storage",
            "Heathrow Extension",
            "Bombardier Replacement Factory",
            "Stennington Hansard Reading Session",
            "Everglades United - Dismantle Quietly",
            "Even After All This Time Ltd",
            "Tuckers Luck - The Big Rerun Project",
            "Brompton Killoff",
            "Stevenage Sewage Sourdour",
            "Coins Bypass",
            "A Nuclear Waste Facility at Hampton Court",
            "Station Platform Reappraisal Study",
            "Shunters Ltd",
            "Strimming Down the Brypatch",
            "Oberon Simmington Takes the Reins",
            "Sheldon Brown's Cycle Tours",
            "Kevlar Nappies for Destructive Babies",
            "Overgrown Pepperpot",
            "Mr Binkles Pantomime at Rashington Theatre - Christmas 2018",
            "IKEA Underpassage - Escape from Mundanity",
            "Poibles",
            "Heagar Lethertube",
            "Fixing the Stantions at Lee Green",
            "PM STTF AR Ltd",
            "Assertion for 1002",
            "Coppermine Discovery - Stranraer Harbour",
            "Oaks",
        ]
        sa_random = [
            # random Strategic Alignment/Key Drivers text, for fun really
            "We need to do this project because it is really important.",
            "We need to do this project because it is really important. People are relying on us.",
            "We think this is a really good idea, actually.",
            "Some things you just need to spend time and money on. This is one.",
            "There more you look at it, the more this project is required to keep the national sanity"
        ]
        for p in range(40):
            portfolio = Portfolio.objects.get(name='DfT Tier 1 Projects')
            # randomness for project names
            project_random_length = len(project_random)
            r = random.choice(range(project_random_length))
            # randomness for key_drivers/strategic alignment
            s_length = len(sa_random)
            sr_random = random.choice(range(s_length))
            p = Project(
                name=project_random[r],
                methodology_id=random.choice([1, 2, 3]),
                sro_id=random.choice(range(1, 21)),
                sro_secondary_id=random.choice(range(1, 21)),
                stage_id=random.choice(range(1, 10)),
                key_drivers=sa_random[sr_random],
                delivery_structure_id=random.choice([1,2]),
                is_gmpp=random.getrandbits(1))
            project_random.pop(r)
            p.save()
            print("Creating project {}".format(p))
            p.portfolios.add(portfolio)


    def _create_quarters(self):
        f1 = Quarter(
            qref='1', yearref='2012', start=date(
                2012, 4, 1), end=date(
                2012, 6, 30))
        f2 = Quarter(
            qref='2', yearref='2012', start=date(
                2012, 7, 1), end=date(
                2012, 9, 30))
        f3 = Quarter(
            qref='3', yearref='2012', start=date(
                2012, 10, 1), end=date(
                2012, 12, 31))
        f4 = Quarter(
            qref='4', yearref='2012', start=date(
                2013, 1, 1), end=date(
                2013, 3, 31))
        f5 = Quarter(
            qref='1', yearref='2013', start=date(
                2013, 4, 1), end=date(
                2013, 6, 30))
        f6 = Quarter(
            qref='2', yearref='2013', start=date(
                2013, 7, 1), end=date(
                2013, 9, 30))
        f7 = Quarter(
            qref='3', yearref='2013', start=date(
                2013, 10, 1), end=date(
                2013, 12, 31))
        f8 = Quarter(
            qref='4', yearref='2013', start=date(
                2014, 1, 1), end=date(
                2014, 3, 31))
        f9 = Quarter(
            qref='1', yearref='2014', start=date(
                2014, 4, 1), end=date(
                2014, 6, 30))
        f10 = Quarter(
            qref='2', yearref='2014', start=date(
                2014, 7, 1), end=date(
                2014, 9, 30))
        f11 = Quarter(
            qref='3', yearref='2014', start=date(
                2014, 10, 1), end=date(
                2014, 12, 31))
        f12 = Quarter(
            qref='4', yearref='2014', start=date(
                2014, 1, 1), end=date(
                2015, 3, 31))
        f13 = Quarter(
            qref='1', yearref='2015', start=date(
                2015, 4, 1), end=date(
                2015, 6, 30))
        f14 = Quarter(
            qref='2', yearref='2015', start=date(
                2015, 7, 1), end=date(
                2015, 9, 30))
        f15 = Quarter(
            qref='3', yearref='2015', start=date(
                2015, 10, 1), end=date(
                2015, 12, 31))
        f16 = Quarter(
            qref='4', yearref='2015', start=date(
                2016, 1, 1), end=date(
                2016, 3, 31))
        f1.save()
        f2.save()
        f3.save()
        f4.save()
        f5.save()
        f6.save()
        f7.save()
        f8.save()
        f9.save()
        f10.save()
        f11.save()
        f12.save()
        f13.save()
        f14.save()
        f15.save()
        f16.save()
        print("Created quarters")


    @staticmethod
    def create_sro_delivery_confidence_rag(proj, quart):
        sdc = DeliveryConfidenceAssessment(project_id=proj.id,
                                           quarter_id=quart.id,
                                           date_assessed=date.today(),
                                           rag_id=random.choice([1, 2, 3, 4, 5]),
                                           granted=True,
                                           assessor_id=random.choice([1, 2, 3, 4, 5]))
        print("Saving SRO Delivery Confidence Assessment: {}".format(sdc.project))
        sdc.save()
        return sdc.id

    def _create_bicc_returns(self):
        all_quarters = Quarter.objects.all()
        all_projects = Project.objects.all()
        returns = [BICCReturn(sro_delivery_confidence_rag_id=Command.create_sro_delivery_confidence_rag(project, quarter))
                   for project in all_projects for quarter in all_quarters]
        for r in returns:
            print(r)
            r.save()

    def handle(self, *args, **options):
        self._create_organisation_type()
        self._create_organisation()
        self._create_organisation_location()
        self._create_organisation_group()
        self._create_organisation_division()
        self._create_organisation_team_type()
        self._create_organisation_team()
        self._create_quarters()
        self._create_portfolios()
        self._create_persons()
        self._create_roles()
        self._create_rags()
        self._create_projects()
        self._create_bicc_returns()
        self._create_project_methodologies()
        self._create_project_stages()
        self._create_delivery_structure()
        self._create_user()
