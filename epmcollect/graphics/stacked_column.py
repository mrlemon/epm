from epmcollect.models import ProjectStage, Quarter


class AggPortfolioDCA(object):
    """
    Class for generating dict/JSON data required by the view producing the graph.

    Use the generate_graph_data method to do the job.
    """

    def generate_graph_data(self):
        """Generates the data required to produce the graph.
        :return: dictionary whose keys are Quarters and values are dictionaries containing the
        count of Projects whose DCA returns (part of a BICCReturn) nominate them as each particular
        RAG value. The dictionary also contains additional data required by the graph (formatted labels,
        etc).

        Example output for a single Quarter:

            "Q3/2015": {"AMBER": 8, "green_label": "GREEN", "label": "Q3/2015", "red_label": "RED", "green_q32015": 13,
                        "red_q32015": 6, "end": "2015-12-31", "amber_q32015": 8, "AMBER/RED": 5,
                        "amber_green_label": "AMBER/GREEN", "amber_label": "AMBER", "start": "2015-10-01",
                        "AMBER/GREEN": 8, "amber_red_label": "AMBER/RED", "GREEN": 13, "amber_green_q32015": 8,
                        "amber_red_q32015": 5, "RED": 6}
        """
        all_quarters = Quarter.objects.all()
        data_dict = {q.get_label(): q.get_rag_count(jsonify=True) for q in all_quarters}
        return data_dict

    def __repr__(self):
        return "AggPortfolioDCA graph"


class DCAProjectMaturity(object):
    """
    Class for generating dict/JSON data required by the view producing the graph.

    Use the generate_graph_data method to do the job.
    """

    def __init__(self, quarter):
        """Most common usage will be to nominate a particular Quarter for which
        to return data.
        :param quarter: """
        self.quarter = quarter

    def generate_graph_data(self):
        """
        Generates the data required to produce the graph.
        :return: dictionary whose keys are ProjectStage objects and values are dictionaries containing
        counts of the number of DCA returns represent a particular RAG value. Also contains other data
        required by the graph (formatted labels, etc). The names are abbreviated (using the abbreviate
        method) if ProjectStage.name is greater than a single word.

        Example of a single ProjectStage:

            "Project Initialisation": {"amber_red_label": "AMBER/RED", "AMBER/RED": 1,
                                              "amber_green_label": "AMBER/GREEN", "label": "PI", "red_label": "RED",
                                              "amber_label": "AMBER", "green_label": "GREEN", "RED": 2}

        """
        all_project_stages = ProjectStage.objects.all()
        data_dict = {ps.name: ps.get_rag_count(quarter=self.quarter) for ps in all_project_stages}
        return data_dict

    def __repr__(self):
        if self.quarter:
            return "DCAProjectMaturityGraph for {}".format(self.quarter)
        else:
            return "DCAProjectmaturityQuarter for all Delivery Confidence Assessments"
