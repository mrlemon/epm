from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import User, Group
from django.contrib.admin import AdminSite
from django.core import urlresolvers

# Register your models here.
from django.utils.html import format_html

from .models import Project, RAG, Person, Role, Portfolio, ProjectMethodology, SRODetails
from .models import Quarter, BICCReturn, DeliveryConfidenceAssessment, ProjectStage, DeliveryStructure
from .models import OrganisationTeamType, OrganisationTeam, OrganisationDivision, OrganisationGroup, Organisation, OrganisationType, OrganisationLocation


class EPMAdmin(AdminSite):
    site_header = "EPM Control"
    index_title = "System administration"

class OrganisationTeamTypeAdmin(admin.ModelAdmin):
    pass

class OrganisationTeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'organisation_division', 'organisation_team_type', 'organisation')
    list_filter = ('organisation_division__organisation_group__organisation',)

    def organisation(self, obj):
        return "{}".format(obj.organisation_division.organisation_group.organisation)

class OrganisationDivisionAdmin(admin.ModelAdmin):
    list_display = ('name', 'organisation_group', 'organisation')
    list_filter = ('organisation_group', 'organisation_group__organisation')

    def organisation(self, obj):
        return "{}".format(obj.organisation_group.organisation)

class OrganisationGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'organisation')

class OrganisationAdmin(admin.ModelAdmin):
    list_display = ('name', 'organisation_type')

class OrganisationTypeAdmin(admin.ModelAdmin):
    pass

class OrganisationLocationAdmin(admin.ModelAdmin):
    list_display = ('address1', 'organisation')

class ProjectStageAdmin(admin.ModelAdmin):
    pass

class ProjectMethodologyAdmin(admin.ModelAdmin):
    pass

class CustomUserAdmin(UserAdmin):
    pass


class CustomGroupAdmin(GroupAdmin):
    pass

class ProjectAdmin(admin.ModelAdmin):

    def get_latest_dca_rag(self):
        """We want the latest DCA RAG rating as a headline here"""
        pass

    fieldsets = (
        ('Basic information', {'fields': ('name', 'description', 'scope', 'owner', 'methodology', 'stage',
                                          'delivery_structure', 'is_gmpp', 'portfolios')}),
        # TODO implement this
#        ('Headline Assessments', {'fields': ('DCA RAG GOES IN HERE',)}),
        ('Drivers', {'fields': ('key_drivers',)})
    )
    filter_horizontal = ('portfolios',)
    list_filter = ('portfolios', 'methodology', 'stage', 'is_gmpp')
    search_fields = ('name',)

class DeliveryConfidenceAssessmentAdmin(admin.ModelAdmin):
    list_display = ('project', 'quarter', 'rag', 'granted')
    list_filter = ('project', 'quarter', 'rag')
    search_fields = ('project',)

class SRODetailsAdmin(admin.ModelAdmin):
    pass

class PersonAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'role')
    list_filter = ('role',)


class PortfolioAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'greens',
                    'amber_greens',
                    'ambers',
                    'amber_reds',
                    'reds')

class QuarterAdmin(admin.ModelAdmin):
    list_filter = ('yearref',)


class BICCReturnAdmin(admin.ModelAdmin):
    list_filter = ('sro_delivery_confidence_rag__quarter',)

class DeliveryStructureAdmin(admin.ModelAdmin):
    pass

# create our own admin class
admin_site = EPMAdmin(name='epmadmin')

admin_site.register(Project, ProjectAdmin)
admin_site.register(Organisation, OrganisationAdmin)
admin_site.register(OrganisationType, OrganisationTypeAdmin)
admin_site.register(OrganisationTeam, OrganisationTeamAdmin)
admin_site.register(OrganisationTeamType, OrganisationTeamTypeAdmin)
admin_site.register(OrganisationGroup, OrganisationGroupAdmin)
admin_site.register(OrganisationDivision, OrganisationDivisionAdmin)
admin_site.register(OrganisationLocation, OrganisationLocationAdmin)
admin_site.register(Person, PersonAdmin)
admin_site.register(Portfolio, PortfolioAdmin)
admin_site.register(RAG)
admin_site.register(Role)
admin_site.register(Quarter, QuarterAdmin)
admin_site.register(BICCReturn, BICCReturnAdmin)
admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)
admin_site.register(DeliveryConfidenceAssessment, DeliveryConfidenceAssessmentAdmin)
admin_site.register(SRODetails, SRODetailsAdmin)
admin_site.register(ProjectMethodology, ProjectMethodologyAdmin)
admin_site.register(ProjectStage, ProjectStageAdmin)
admin_site.register(DeliveryStructure, DeliveryStructureAdmin)
