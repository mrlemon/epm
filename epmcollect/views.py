import json

from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from epmcollect.forms import NewDCAForm
from epmcollect.graphics.stacked_column import DCAProjectMaturity, AggPortfolioDCA
from .models import Project, Quarter, DeliveryConfidenceAssessment, BICCReturn


# Create your views here.

# ------CLASS_BASED_VIEWS------------#

class ProjectListView(ListView):
    template_name = 'epmcollect/projects.html'
    model = Project
    context_object_name = 'all_projects'

    def get_queryset(self):
        return Project.objects.all()


class ProjectDetailView(DetailView):
    template_name = 'epmcollect/single_project.html'
    model = Project

    def get_context_data(self, **kwargs):
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        project = kwargs['object']
        context['delivery_confidence_assessments'] = project.epmcollect_deliveryconfidenceassessment_related.all()
        context['project_data'] = project
        return context


class DeliveryConfidenceAssessmentView(DetailView):
    template_name = 'epmcollect/dca.html'
    context_object_name = 'dca_data'
    model = DeliveryConfidenceAssessment


class DeliveryConfidenceAssessmentCreate(CreateView):
    model = DeliveryConfidenceAssessment
    template_name = 'epmcollect/forms/dca-new.html'
    fields = ['project', 'quarter', 'rag', 'assessor', 'date_assessed', 'overall_commentary']


class BICCReturnList(ListView):
    model = BICCReturn
    context_object_name = 'bicc_returns'


class BICCReturnDetailView(DetailView):
    model = BICCReturn

    def get_context_data(self, **kwargs):
        context = super(BICCReturnDetailView, self).get_context_data(**kwargs)
        bicc_return = kwargs['object']
        context['bicc_return'] = bicc_return
        return context


# ------FUNCTION-BASED_VIEWS---------#

def index(request):
    user = request.user.username
    owned_projects = Project.objects.filter(owner__username=user)
    context = {
        'user': user,
        'owned_projects': owned_projects
    }
    return render(request, 'epmcollect/index.html', context)


def login(request):
    return render(request, 'registration/login.html')


def aggregate_returns_rag_per_quarter(request):
    graph_object = AggPortfolioDCA()
    data = graph_object.generate_graph_data()
    data_dict = json.dumps(data)

    context = {
        'view_json': data_dict
    }
    return render(request, 'epmcollect/single_graph.html', context)


def delivery_confidence_against_project_maturity(request, yearref, qref):
    q = Quarter.objects.get(yearref=yearref, qref=qref)
    graph_object = DCAProjectMaturity(q)
    data = graph_object.generate_graph_data()
    json_data = json.dumps(data)
    context = {
        'view_json': json_data,
        'quarter_label': q.get_label()
    }
    return render(request, 'epmcollect/single_graph_dca_maturity.html', context)


def dca_add(request, project_id):
    if request.method == 'POST':
        form = NewDCAForm(request.POST)
        if form.is_valid():
            quarter = form.cleaned_data['quarter']
            date_assessed = form.cleaned_data['date_assessed']
            assessor = form.cleaned_data['assessor']
            rag = form.cleaned_data['rag']
            overall_commentary = form.cleaned_data['overall_commentary']

            # process the request:
            dca = DeliveryConfidenceAssessment(
                project_id=project_id,
                quarter=quarter,
                date_assessed=date_assessed,
                assessor=assessor,
                rag=rag,
                overall_commentary=overall_commentary
            )
            dca.save()

            return HttpResponseRedirect('/epmcollect/thanks/')
    else:
        form = NewDCAForm()
        project = Project.objects.get(id=project_id).name
    return render(request, 'epmcollect/forms/dca-new.html', {'form': form, 'project': project})


def thanks(request):
    return render(request, 'epmcollect/thanks.html')


def bicc_pre_submit_summary(request, project_id):
    project = Project.objects.get(id=project_id)
    ungranted_assessments = [dca for dca in project.epmcollect_deliveryconfidenceassessment_related.all()
                             if dca.granted == False]
    context = {
        'project': project,
        'ungranted_assessments': ungranted_assessments
    }
    return render(request, 'epmcollect/bicc_preview.html', context)


def bicc_new(request):
    if request.method == 'POST':
        dca_id = request.POST.get('dca_id')
        # first we grant the dca
        dca = DeliveryConfidenceAssessment.objects.get(id=dca_id)
        dca.granted = True
        dca.save()
        # now we create and save the BICC return with this razzer in it
        bicc_return = BICCReturn(sro_delivery_confidence_rag=dca)
        bicc_return.save()
        return render(request, 'epmcollect/bicc_new_confirm.html', {'bicc_return': bicc_return})
