from django import forms
from epmcollect.models import DeliveryConfidenceAssessment

class NewDCAForm(forms.ModelForm):
    class Meta:
        model = DeliveryConfidenceAssessment
        fields = ['quarter', 'date_assessed', 'assessor', 'rag', 'overall_commentary']