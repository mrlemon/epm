def number_of_rags(portfolio, rag_type):
    """
    Calculate the number of projects rated as a particular RAG within a portfolio.
    :param portfolio:
    :param rag_type:
    :return: integer
    """
    from .models import Project
    return len(Project.objects.filter(portfolios__name=portfolio, sro_rating__rag=rag_type))


def rag_labels():
    """
    Function to return RAG labels for use in graphs or wherever.
    :return: dictionary of rag labels
    """
    from .models import RAG
    green_label = RAG.objects.filter(rag='GREEN')[0].rag
    amber_green_label = RAG.objects.filter(rag='AMBER/GREEN')[0].rag
    amber_label = RAG.objects.filter(rag='AMBER')[0].rag
    amber_red_label = RAG.objects.filter(rag='AMBER/RED')[0].rag
    red_label = RAG.objects.filter(rag='RED')[0].rag
    d = dict(
        green_label=green_label,
        amber_green_label=amber_green_label,
        amber_label=amber_label,
        amber_red_label=amber_red_label,
        red_label=red_label
    )
    return d


def ready_graph_context_for_json(dictionary, label):
    """
    The purpose of this function is to return a dict of
    :param label: a string for the string representation of the quarter
    (e.g. 'q1/2012'
    :type dictionary: dict to be treated
    """
    d = {}
    for k, v in dictionary.items():
        k = k.replace("/", "_")
        d[k.lower() + '_' + label['label']] = v
    # include the original label dict in there too
    d.update(dictionary)
    return d


def gen_dft_id(db_id):
    """
    The purpose of this function is to be used by the Project model class when it is creating a new
    Project. This function gets called and the ID gets automatically allocated.
    :return: String: the ID
    """
    s = 'DFT_PORTFOLIO_{}'.format(str(db_id).zfill(4))
    return s




