# -*- coding: utf-8 -*-
"""
    epm.models
    ~~~~~~~~~~

    DESCRIPTION

    :copyright: (c) 2016 by MATTHEW LEMON.
    :license: MIT, see LICENSE for more details.
"""
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import models

from .utils import number_of_rags, ready_graph_context_for_json, rag_labels, gen_dft_id


# Create your models here.


class OrganisationType(models.Model):
    """
    Organisations in this system can be of various types.
    """
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Organisaton Types'

    def __str__(self):
        return "{}".format(self.name)


class Organisation(models.Model):
    """
    Standard organisation e.g. DfT, Highways Agency
    """
    organisation_type = models.ForeignKey(OrganisationType)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)

    def __str__(self):
        return "{}".format(self.name)

class OrganisationLocation(models.Model):
    """
    Organisations can have various locations (offices) so we need separation.
    """
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    address1 = models.CharField(max_length=50)
    address2 = models.CharField(max_length=50, null=True, blank=True)
    address3 = models.CharField(max_length=50, null=True, blank=True)
    address4 = models.CharField(max_length=50, null=True, blank=True)
    address5 = models.CharField(max_length=50, null=True, blank=True)
    postcode = models.CharField(max_length=10, null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Organisation Locations'

    def __str__(self):
        return "Address at for {}".format(self.address1)

class OrganisationGroup(models.Model):
    """
    Like a DfT Group. Not all will have them so they are only linked to team
    as an option. An OrganisationTeam links straight to an Organisation
    """
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Organisation Groups'

    def __str__(self):
        return "{} - {}".format(self.name, self.organisation)

class OrganisationDivision(models.Model):
    """
    Like Governance Division.
    """
    organisation_group = models.ForeignKey(OrganisationGroup, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Organisation Divisions'

    def __str__(self):
        return "{}".format(self.name)


class OrganisationTeamType(models.Model):
    """
    Type of a team. Project, Programme, Admin, etc
    """
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Organisation Team Types'

    def __str__(self):
        return "{}".format(self.name)


class OrganisationTeam(models.Model):
    """
    A team in an organisation, like Portfolio Team.
    """
    organisation_division = models.ForeignKey(OrganisationDivision, on_delete=models.CASCADE)
    organisation_team_type = models.ForeignKey(OrganisationTeamType, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Organisation Teams'

    def __str__(self):
        return "{}".format(self.name)


class ProjectStage(models.Model):
    """
    EPM ProjectStage class - used in the Milestones and Assurance return.
    """
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=1000, null=True, blank=True, default='')

    class Meta:
        verbose_name = 'Project Stage'

    def abbreviate(self):
        """
        For some ProjectStage reprs, we need to return an abbreviation
        rather than the full string.
        :return:
        """
        try:
            s_list = self.name.split(' ')
            if len(s_list) > 1:
                abbrev = ''.join([x[0] for x in s_list])
                return abbrev
            else:
                return self.name
        except:
            return self.name

    def get_rag_count(self, quarter=None):
        """We need to get the count of RAGs for each ProjectStage, by Quarter optionally."""

        # this is how to do a nested list comp properly in Python
        # the big loop goes first, then the inner loop after
        if quarter:
            bicc_returns = [
                dca.biccreturn_set.first()
                for p in self.project_set.all()  # BIG loop
                for dca in p.epmcollect_deliveryconfidenceassessment_related.all() if
                dca.granted is True and dca.quarter == quarter  # inner loop
                ]
        else:
            bicc_returns = [
                dca.biccreturn_set.first()
                for p in self.project_set.all()  # BIG loop
                for dca in p.epmcollect_deliveryconfidenceassessment_related.all() if dca.granted == True
                ]

        rags = [r.sro_delivery_confidence_rag.rag.rag for r in bicc_returns]
        from collections import Counter
        count_rags = Counter(rags)
        labels = rag_labels()
        label = {'label': self.abbreviate()}
        context = {}
        context.update(count_rags)
        context.update(labels)
        context.update(label)
        return context

    def __repr__(self):
        return self.name

    def __str__(self):
        return "{}".format(self.name)


class ProjectMethodology(models.Model):
    """
    EPM ProjectMethodology class, for including definitions of 'waterfall', 'agile', etc.
    """
    methodology = models.CharField(max_length=30)

    class Meta:
        verbose_name_plural = "Project Methodologies"

    def __str__(self):
        return "{}".format(self.methodology)


class ConfidenceAssessmentBase(models.Model):
    """
    EPM ConfidenceAssessment base class, to be subclassed by other assessment classes.
    As an abstract class, this does not get a database table and it cannot be instantiated.
    """
    project = models.ForeignKey('Project', on_delete=models.CASCADE, related_name="%(app_label)s_%(class)s_related")
    quarter = models.ForeignKey('Quarter', on_delete=models.CASCADE, related_name="%(app_label)s_%(class)s_related")
    date_assessed = models.DateField()
    assessor = models.ForeignKey('Person', on_delete=models.CASCADE, related_name="%(app_label)s_%(class)s_related")
    granted = models.BooleanField(default=False)  # in honour of Will Grant

    class Meta:
        abstract = True


class DeliveryConfidenceAssessment(ConfidenceAssessmentBase):
    """
    EPM DeliveryConfidenceAssessment class.
    """
    rag = models.ForeignKey('RAG')
    overall_commentary = models.TextField(max_length=2000)

    class Meta:
        verbose_name_plural = "Delivery Confidence Assessments"

    def get_absolute_url(self):
        return reverse('epmcollect:dca', kwargs={'pk': self.pk})

    def __str__(self):
        return "DC for {} for {}".format(self.project, self.quarter)


# TODO implement FinanceConfidenceAssessment and BenefitsConfidenceAssessment classes here


class Portfolio(models.Model):
    """
    EPM Portfolio model
    """
    name = models.CharField(max_length=30)

    @property
    def greens(self):
        return number_of_rags(self.name, "GREEN")

    @property
    def amber_greens(self):
        return number_of_rags(self.name, "AMBER/GREEN")

    @property
    def ambers(self):
        return number_of_rags(self.name, "AMBER")

    @property
    def amber_reds(self):
        return number_of_rags(self.name, "AMBER/RED")

    @property
    def reds(self):
        return number_of_rags(self.name, "RED")

    def __str__(self):
        return self.name


class Role(models.Model):
    """
    EPM Role model
    """
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class PLPStatus(models.Model):
    """
    Services SRODetails model.
    """
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    why_not_mpla = models.CharField(max_length=1000, verbose_name='Why not MPLA?')

    def __str__(self):
        return self.name


class MPLAStatus(models.Model):
    """
    Self-explanatory. Services the SRODetails model.
    """
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    why_not_plp = models.CharField(max_length=1000, verbose_name='Why not PLP?')

    def __str__(self):
        return self.name


class SRODetails(models.Model):
    """
    Additional details for the Person if they are an SRO.
    """
    sro_person = models.ForeignKey('Person', on_delete=models.CASCADE, verbose_name='SRO')
    pass
    sro_tenure_start = models.DateField(null=True, verbose_name='SRO Tenure Start')
    sro_letter_issued = models.BooleanField(default=True, verbose_name='SRO Letter Issued')
    sro_letter_issued_date = models.DateField(null=True, verbose_name='SRO Letter Issued Date')
    sro_tenure_end = models.DateField(null=True, verbose_name='SRO Tenure End', blank=True)
    percentage_time_in_role = models.IntegerField(default=100, verbose_name='Percentage time in SRO role', blank=True)
    additional_information_about_role = models.CharField(max_length=1000, default='', null=True, blank=True)
    mpla_status = models.ForeignKey(MPLAStatus, null=True, verbose_name='MPLA status', blank=True)
    plp_status = models.ForeignKey(PLPStatus, null=True, verbose_name='PLP status', blank=True)

    class Meta:
        verbose_name = 'SRO Details'
        verbose_name_plural = 'SRO Details'

    def __str__(self):
        try:
            return "SRO Details for {}".format(self.sro_person)
        except ObjectDoesNotExist:
            return "No Person!"


class Person(models.Model):
    """
    EPM Person model
    """
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    team = models.ForeignKey(OrganisationTeam, on_delete=models.SET_NULL, null=True)
    landline = models.CharField(max_length=50, null=True)
    landline_alt = models.CharField(max_length=50, verbose_name='Landline (Alternative)', null=True)
    mobile = models.CharField(max_length=50, null=True)
    email = models.EmailField(null=True)
    email_alt = models.EmailField(verbose_name='Email (Alternative)', null=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, null=True)
    sro_role_details = models.OneToOneField(SRODetails, null=True, on_delete=models.CASCADE, verbose_name='SRO Details')

    def _get_full_name(self):
        """Returns a person's full name."""
        return '%s %s' % (self.first_name, self.last_name)

    def _get_project(self):
        """Returns the project someone is on."""
        project = self.project_set.all()[0]
        return '%s' % project.name

    full_name = property(_get_full_name)
    project = property(_get_project)

    def __str__(self):
        return self.first_name + " " + self.last_name

    class Meta:
        verbose_name_plural = "People"


class RAG(models.Model):
    """
    EPM RAG model
    """
    rag = models.CharField(max_length=12)

    def __str__(self):
        return "{}".format(self.rag)

    class Meta:
        verbose_name = "RAG"
        verbose_name_plural = "RAG ratings"


class DeliveryStructure(models.Model):
    """
    Delivery Structure (in GMPP terms). Either a project or a programme, basically.
    """
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=500)

    class Meta:
        verbose_name_plural = 'Delivery Structures'

    def __str__(self):
        return "{}".format(self.name)


class Project(models.Model):
    """
    EPM Project model
    """
    name = models.CharField(max_length=200)
    sro = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, verbose_name="SRO")
    sro_secondary = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, verbose_name="Secondary SRO",
                                      related_name='project_secondary_sro')
    portfolios = models.ManyToManyField(Portfolio, related_name='projects', verbose_name="Portfolio")
    methodology = models.ForeignKey(ProjectMethodology, related_name='project_methodology',
                                    verbose_name="Project Methodology")
    owner = models.ForeignKey(User, null=True)
    stage = models.ForeignKey(ProjectStage, null=True)
    # TODO - check whether we should keep key_drivers as a text field or whether we need to separate these out into a table
    key_drivers = models.CharField(verbose_name='Strategic Alignment/Key Drivers', default='', null=True,
                                   max_length=1000)
    delivery_structure = models.ForeignKey(DeliveryStructure, null=True, verbose_name='Delivery Structure')
    is_gmpp = models.BooleanField(default=False, verbose_name='GMPP')
    formal_id = models.CharField(default='', verbose_name='Formal ID', max_length=20, editable=False)
    scope = models.CharField(default='', max_length=1000, null=True)
    description = models.CharField(default='', max_length=1000, null=True)

    @property
    def bicc_returns(self):
        """
        Get a list of BICCReturn objects for a Project. Call count() method
        on it to get number of returns, etc.
        :return: list of BICCReturn objects.
        """
        return [dca.biccreturn_set.first() for dca in self.epmcollect_deliveryconfidenceassessment_related.all()]

    def __str__(self):
        return "{}".format(self.name)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Project, self).save()
        # horrible that we have to save the instance first to get its id...
        self.formal_id = gen_dft_id(self.id)
        super(Project, self).save()


class Quarter(models.Model):
    """
    EPM Quarter model
    """
    qref = models.CharField(max_length=1, verbose_name='Quarter')
    yearref = models.CharField(max_length=4, verbose_name="Year")
    start = models.DateField()
    end = models.DateField()

    def get_label(self, export_format=None):
        """
        Provides a handy label for the quarter (e.g. "Q1/2012")
        :param export_format: pass 'graphs' if you want labels to be exported
        as a dictionary in the form {'label': 'q12012'} which is used for later
        json conversion in the view, for passing to Javascript functions.
        :type export_format: string
        :return: string version of the object identifier
        """
        if export_format == 'lower_case':
            return dict(label="q{0}{1}".format(self.qref, self.yearref))
        else:
            return "Q{0}/{1}".format(self.qref, self.yearref)

    @property
    def bicc_returns(self):
        """
        Get a list of BICCReturn objects for a Quarter. Call count() method
        on it to get number of returns, etc.
        :return: list of BICCReturn objects.
        """
        return [dca.biccreturn_set.first() for dca in self.epmcollect_deliveryconfidenceassessment_related.all()]

    def get_rag_count(self, jsonify=False):
        """
        Get a count of each RAG rating for a BICCReturn object, e.g. {'AMBER':10 ,
        'GREEN': 8} and so on.
        :param: jsonify if True, will jsonify the resulting dict for passing to a template.
        False will return a simple dict.
        :return: dictionary of rag : count pairs
        """
        # TODO fix this
        bicc_returns = [dca.biccreturn_set.first() for dca in
                        self.epmcollect_deliveryconfidenceassessment_related.all() if dca.granted == True]
        start = dict(start=self.start.isoformat())
        end = dict(end=self.end.isoformat())
        label = dict(label=self.get_label())
        rags = [r.sro_delivery_confidence_rag.rag.rag for r in bicc_returns]
        from collections import Counter
        count_rags = Counter(rags)
        if jsonify:
            labels = rag_labels()
            context = ready_graph_context_for_json(count_rags, self.get_label(export_format='lower_case'))
            context.update(labels)
            context.update(start)
            context.update(end)
            context.update(label)
            return context
        else:
            return dict(count_rags)

    def __str__(self):
        return "Q%s/%s" % (self.qref, self.yearref)


class BICCReturn(models.Model):
    """
    EPM BICC Return model
    """
    sro_delivery_confidence_rag = models.ForeignKey(DeliveryConfidenceAssessment, on_delete=models.CASCADE,
                                                    verbose_name="SRO Delivery Confidence RAG")

    def __str__(self):
        return "BICC_Return: {} for {}".format(self.sro_delivery_confidence_rag.quarter,
                                               self.sro_delivery_confidence_rag.project)

    class Meta:
        verbose_name = "BICC Return"
        verbose_name_plural = "BICC Returns"
