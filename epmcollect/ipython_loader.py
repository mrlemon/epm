from epmcollect.models import Person, Portfolio, Project, BICCReturn, RAG
from epmcollect.models import Quarter, Role, DeliveryConfidenceAssessment

from datetime import date

# objective is to use this file to easily lead fixture stuff for the ipython console
print("Creating objects to use....")
ipy_role = Role.objects.get(id=1)
ipy_person = Person.objects.get(id=1)
ipy_portfolio = Portfolio.objects.get(id=1)
ipy_rag = RAG.objects.get(id=1)
ipy_project = Project.objects.get(id=1)
ipy_quarter = Quarter.objects.get(id=1)
ipy_dca = DeliveryConfidenceAssessment(
    assessor=ipy_person,
    date_assessed=date.today(),
    project=ipy_project,
    quarter=ipy_quarter,
    rag=ipy_rag)
ipy_br = BICCReturn(sro_delivery_confidence_rag=ipy_dca)
