// a global variable to be populated by the columnSort() function, which can be
// used by the aggregateRAGChartMulti() function.
var sortedColumns = [];

function columnSort() {
    /**
     * The objective of this function is to transform the viewJSON object that came from
     * the Python view function, into an array, and sort it by the start-date of the particular
     * quarter. This is then passed to the global sortedColumns variable which is used
     * in the graphics function.
     */
    "use strict";
    var dataArray = [];
    console.log(viewJSON);
    $.map(viewJSON, function (v, i) {
        dataArray.push(v);
    });
    dataArray.sort(function (a, b) {
        // here we sort by date, so that earliest is first
        return Date.parse(a.start) - Date.parse(b.start);
    });
    // then we set the global variable so that the aggregateRAGChartMulti function can use
    sortedColumns = dataArray;
}

/**
 * This function draws the graph, using the sorted array whose original data comes from the
 * viewJSON object that is sent by the Python view
 */
function DCAChart() {
    "use strict";
    var dataArray = [];
    console.log(viewJSON);
    $.map(viewJSON, function (v) {
        dataArray.push(v);
    });

    $('#agg_rag_multi').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Delivery Confidence Against Project Maturity - ' + quarterLabel
        },
        xAxis: {
            categories: [
                dataArray[0]['label'],
                dataArray[1]['label'],
                dataArray[2]['label'],
                dataArray[3]['label'],
                dataArray[4]['label'],
                dataArray[5]['label'],
                dataArray[6]['label'],
                dataArray[7]['label'],
                dataArray[8]['label']
            ]
        },
        colors: [
            /**
             * These are hard-coded for now
             */
            '#EB1509',
            '#EBA309',
            '#EBD009',
            '#AACC00',
            '#00CC25',
        ],
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Project Returns'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -40,
            verticalAlign: 'bottom',
            y: 20,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
            formatter: function () {
                var s = '<strong>' + this.x + '</strong><br>';
                s += '<br><strong>Projects:</strong> ' + this.y ;
                s += '<br><strong>Percentage:</strong> ' + Math.floor(this.percentage) + '%';
                return s;
            }

        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        var label = Math.floor(this.percentage) + '%';
                        return label;
                    },
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            // the '_label' element is actually present in every
            // element of the sorted array, but we'll just use the
            // one from the first ([0]) array for ease of use.
            name: dataArray[0]['red_label'],
            data: [
                /**
                 * These return the number values which are the count of
                 * each RAG in each return. Again, we are going for eight
                 * column in this particular form, and starting with the earliest,
                 * hence length -8.
                 * TODO We need a function to allow for choice in number of columns to show
                 */
                parseInt(dataArray[0]['RED']),
                parseInt(dataArray[1]['RED']),
                parseInt(dataArray[2]['RED']),
                parseInt(dataArray[3]['RED']),
                parseInt(dataArray[4]['RED']),
                parseInt(dataArray[5]['RED']),
                parseInt(dataArray[6]['RED']),
                parseInt(dataArray[7]['RED']),
                parseInt(dataArray[8]['RED']),
            ]
        }, {
            name: dataArray[0]['amber_red_label'],
            data: [
                parseInt(dataArray[0]['AMBER/RED']),
                parseInt(dataArray[1]['AMBER/RED']),
                parseInt(dataArray[2]['AMBER/RED']),
                parseInt(dataArray[3]['AMBER/RED']),
                parseInt(dataArray[4]['AMBER/RED']),
                parseInt(dataArray[5]['AMBER/RED']),
                parseInt(dataArray[6]['AMBER/RED']),
                parseInt(dataArray[7]['AMBER/RED']),
                parseInt(dataArray[8]['AMBER/RED']),
            ]
        }, {
            name: dataArray[0]['amber_label'],
            data: [
                parseInt(dataArray[0]['AMBER']),
                parseInt(dataArray[1]['AMBER']),
                parseInt(dataArray[2]['AMBER']),
                parseInt(dataArray[3]['AMBER']),
                parseInt(dataArray[4]['AMBER']),
                parseInt(dataArray[5]['AMBER']),
                parseInt(dataArray[6]['AMBER']),
                parseInt(dataArray[7]['AMBER']),
                parseInt(dataArray[8]['AMBER']),
            ]
        }, {
            name: dataArray[0]['amber_green_label'],
            data: [
                parseInt(dataArray[0]['AMBER/GREEN']),
                parseInt(dataArray[1]['AMBER/GREEN']),
                parseInt(dataArray[2]['AMBER/GREEN']),
                parseInt(dataArray[3]['AMBER/GREEN']),
                parseInt(dataArray[4]['AMBER/GREEN']),
                parseInt(dataArray[5]['AMBER/GREEN']),
                parseInt(dataArray[6]['AMBER/GREEN']),
                parseInt(dataArray[7]['AMBER/GREEN']),
                parseInt(dataArray[8]['AMBER/GREEN']),
            ]
        }, {
            name: dataArray[0]['green_label'],
            data: [
                parseInt(dataArray[0]['GREEN']),
                parseInt(dataArray[1]['GREEN']),
                parseInt(dataArray[2]['GREEN']),
                parseInt(dataArray[3]['GREEN']),
                parseInt(dataArray[4]['GREEN']),
                parseInt(dataArray[5]['GREEN']),
                parseInt(dataArray[6]['GREEN']),
                parseInt(dataArray[7]['GREEN']),
                parseInt(dataArray[8]['GREEN']),
            ]
        }]
    });
}
function aggregateRAGChartMulti() {
    "use strict";

    $('#agg_rag_multi').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aggregate RAG scores for projects per quarter'
        },
        xAxis: {
            /**
             * the categories are the names of the columns. In this
             * case, we want it in the form "Q1/2014", which is an element
             * from the viewJSON object that is sent from the Python view
             * function.
             */
            categories: [
                /**
                 * This form is requesting the column objects in reverse order
                 * so that we get the oldest first (the farthest back in the 
                 * sortedColumns array.
                 */
                sortedColumns[sortedColumns.length -8]['label'], 
                sortedColumns[sortedColumns.length -7]['label'], 
                sortedColumns[sortedColumns.length -6]['label'],
                sortedColumns[sortedColumns.length -5]['label'],
                sortedColumns[sortedColumns.length -4]['label'],
                sortedColumns[sortedColumns.length -3]['label'],
                sortedColumns[sortedColumns.length -2]['label'],
                sortedColumns[sortedColumns.length -1]['label']
                ]
        },
        colors: [
            /**
             * These are hard-coded for now
             */
            '#EB1509',
            '#EBA309',
            '#EBD009',
            '#AACC00',
            '#00CC25',
        ],
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Project Returns'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -40,
            verticalAlign: 'bottom',
            y: 20,
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}',
            formatter: function () {
                var s = '<strong>' + this.x + '</strong><br>';
                s += '<br><strong>Projects:</strong> ' + this.y ;
                s += '<br><strong>Percentage:</strong> ' + Math.floor(this.percentage) + '%';
                return s;
            }
            
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        var label = Math.floor(this.percentage) + '%';
                        return label;
                    },
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                }
            }
        },
        series: [{
            // the '_label' element is actually present in every
            // element of the sorted array, but we'll just use the
            // one from the first ([0]) array for ease of use.
            name: sortedColumns[0]['red_label'],
            data: [
                /**
                 * These return the number values which are the count of
                 * each RAG in each return. Again, we are going for eight
                 * column in this particular form, and starting with the earliest,
                 * hence length -8.
                 * TODO We need a function to allow for choice in number of columns to show
                 */
                parseInt(sortedColumns[sortedColumns.length -8]['RED']),
                parseInt(sortedColumns[sortedColumns.length -7]['RED']),
                parseInt(sortedColumns[sortedColumns.length -6]['RED']),
                parseInt(sortedColumns[sortedColumns.length -5]['RED']),
                parseInt(sortedColumns[sortedColumns.length -4]['RED']),
                parseInt(sortedColumns[sortedColumns.length -3]['RED']),
                parseInt(sortedColumns[sortedColumns.length -2]['RED']),
                parseInt(sortedColumns[sortedColumns.length -1]['RED']),
            ]
        }, {
            name: sortedColumns[0]['amber_red_label'],
            data: [
                parseInt(sortedColumns[sortedColumns.length -8]['AMBER/RED']),
                parseInt(sortedColumns[sortedColumns.length -7]['AMBER/RED']),
                parseInt(sortedColumns[sortedColumns.length -6]['AMBER/RED']),
                parseInt(sortedColumns[sortedColumns.length -5]['AMBER/RED']),
                parseInt(sortedColumns[sortedColumns.length -4]['AMBER/RED']),
                parseInt(sortedColumns[sortedColumns.length -3]['AMBER/RED']),
                parseInt(sortedColumns[sortedColumns.length -2]['AMBER/RED']),
                parseInt(sortedColumns[sortedColumns.length -1]['AMBER/RED']),
            ]
        }, {
            name: sortedColumns[0]['amber_label'],
            data: [
                parseInt(sortedColumns[sortedColumns.length -8]['AMBER']),
                parseInt(sortedColumns[sortedColumns.length -7]['AMBER']),
                parseInt(sortedColumns[sortedColumns.length -6]['AMBER']),
                parseInt(sortedColumns[sortedColumns.length -5]['AMBER']),
                parseInt(sortedColumns[sortedColumns.length -4]['AMBER']),
                parseInt(sortedColumns[sortedColumns.length -3]['AMBER']),
                parseInt(sortedColumns[sortedColumns.length -2]['AMBER']),
                parseInt(sortedColumns[sortedColumns.length -1]['AMBER']),
            ]
        }, {
            name: sortedColumns[0]['amber_green_label'],
            data: [
                parseInt(sortedColumns[sortedColumns.length -8]['AMBER/GREEN']),
                parseInt(sortedColumns[sortedColumns.length -7]['AMBER/GREEN']),
                parseInt(sortedColumns[sortedColumns.length -6]['AMBER/GREEN']),
                parseInt(sortedColumns[sortedColumns.length -5]['AMBER/GREEN']),
                parseInt(sortedColumns[sortedColumns.length -4]['AMBER/GREEN']),
                parseInt(sortedColumns[sortedColumns.length -3]['AMBER/GREEN']),
                parseInt(sortedColumns[sortedColumns.length -2]['AMBER/GREEN']),
                parseInt(sortedColumns[sortedColumns.length -1]['AMBER/GREEN']),
            ]
        }, {
            name: sortedColumns[0]['green_label'],
            data: [
                parseInt(sortedColumns[sortedColumns.length -8]['GREEN']),
                parseInt(sortedColumns[sortedColumns.length -7]['GREEN']),
                parseInt(sortedColumns[sortedColumns.length -6]['GREEN']),
                parseInt(sortedColumns[sortedColumns.length -5]['GREEN']),
                parseInt(sortedColumns[sortedColumns.length -4]['GREEN']),
                parseInt(sortedColumns[sortedColumns.length -3]['GREEN']),
                parseInt(sortedColumns[sortedColumns.length -2]['GREEN']),
                parseInt(sortedColumns[sortedColumns.length -1]['GREEN']),
            ]
        }]
    });
}


function aggregateRAGChart() {
    "use strict";
    var green_label = viewJSON['green_label'];
    var amber_green_label = viewJSON['amber_green_label'];
    var amber_label = viewJSON['amber_label'];
    var amber_red_label = viewJSON['amber_red_label'];
    var red_label = viewJSON['red_label'];

    var greens = viewJSON['GREEN'];
    var amber_greens = viewJSON['AMBER/GREEN'];
    var ambers = viewJSON['AMBER'];
    var amber_reds = viewJSON['AMBER/RED'];
    var reds = viewJSON['RED'];
    
    var categories;
    categories = ['TEST DATA'];
    
    $('#agg-rag-per-quarter').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aggregrate RAG scores per project per quarter'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Percentage of Projects'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        colors: [
            '#00CC25',
            '#AACC00',
            '#EBD009',
            '#EBA309',
            '#EB1509'
        ],
        series: [{
            name: green_label,
            data: [parseInt(greens)]
        }, {
            name: amber_green_label,
            data: [parseInt(amber_greens)]
        }, {
            name: amber_label,
            data: [parseInt(ambers)]
        }, {
            name: amber_red_label,
            data: [parseInt(amber_reds)]
        }, {
            name: red_label,
            data: [parseInt(reds)]
        }]
    });
}


function singleColumnAggRagQuarter() {
    "use strict";
    var q_data_label = viewJSON['q_data_label'];
    var green_label = viewLabels['green_label'];
    var greens = viewJSON['greens'];
    var amber_green_label = viewLabels['amber_green_label'];
    var amber_greens = viewJSON['amber_greens'];
    var ambers = viewJSON['ambers'];
    var amber_label = viewLabels['amber_label'];
    var amber_reds = viewJSON['amber_reds'];
    var amber_red_label = viewLabels['amber_red_label'];
    var reds = viewJSON['reds'];
    var red_label = viewLabels['red_label'];
    var categories;
    categories = [q_data_label];
    $('#agg-rag-per-quarter').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aggregrate RAG scores per project per quarter'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Percentage of Projects'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        colors: [
            '#00CC25',
            '#AACC00',
            '#EBD009',
            '#EBA309',
            '#EB1509'
        ],
        series: [{
            name: green_label,
            data: [parseInt(greens)]
        }, {
            name: amber_green_label,
            data: [parseInt(amber_greens)]
        }, {
            name: amber_label,
            data: [parseInt(ambers)]
        }, {
            name: amber_red_label,
            data: [parseInt(amber_reds)]
        }, {
            name: red_label,
            data: [parseInt(reds)]
        }]
    });
}
